cmake_minimum_required(VERSION 3.19.1)
set(CMAKE_TOOLCHAIN_FILE ~/dev/vcpkg/scripts/buildsystems/vcpkg.cmake)
project (main)

set(CMAKE_CXX_STANDARD 17)
find_package(CURL CONFIG REQUIRED)

set(SOURCES main.cpp ./HttpHelper.h)

add_executable(main ${SOURCES})
target_include_directories(main PRIVATE .)
target_link_libraries(main PRIVATE CURL::libcurl)
