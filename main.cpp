#include <iostream>
// #include "HttpHelper.h"
#include <curl/curl.h>

using std::cerr;
using std::cout;
using std::endl;

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
  return size * nmemb;
}

int main(void)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if (curl)
  {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    /* example.com is redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* so it will not print responses in console */
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    /* 0 disable messages */
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);

    /* perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* check for errors */
    if (res == CURLE_OK)
      cout << "Downloaded successfully" << endl;
    else
      cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << endl;

    /* always cleanup */
    curl_easy_cleanup(curl);
  }

  return 0;
}
